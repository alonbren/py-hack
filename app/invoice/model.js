import DS from 'ember-data';

export default DS.Model.extend({
  companyName: DS.attr("string"),
  fullName: DS.attr("string"),
  country: DS.attr("string"),
  state: DS.attr("string"),
  invoiceNumber: DS.attr("string"),
  dueDate: DS.attr("date"),
  currency: DS.attr("string"),
  amount: DS.attr("number"),
  description: DS.attr("string")
});
