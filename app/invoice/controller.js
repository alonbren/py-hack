import Ember from 'ember';

export default Ember.Controller.extend({
  step: 1,
  status: true,
  isUploadStep: Ember.computed('step', function() {
    return this.get('step') === 1;
  }),
  isSumStep: Ember.computed('step', function() {
    return this.get('step') === 2;
  }),
  isSuccessStep: Ember.computed('step', function() {
    return this.get('step') === 3;
  }),
  isNotSuccessStep: Ember.computed('step', function() {
    return this.get('step') !== 3;
  }),
  fileAdded: false,
  actions: {
    success: function(file, responseText) {
      console.dir(responseText);
      this.get('model').setProperties({
        companyName: responseText.CompanyName,
        fullName: responseText.FullName,
        amount: responseText.Amount,
        currency: responseText.Currency,
        dueDate: new Date(responseText.DueDate),
        invoiceNumber: responseText.InvoiceNumber
      });
      this.set('step', 2);
    },
    error: function(file, errorMessage, xmlHttpRequest) {
      console.dir(errorMessage);
      this.set('step', 2);
    }
  }

});
