import Ember from 'ember';

export default Ember.Component.extend({
  actions: {
    addedFileEvent: function () {
      this.set('fileAdded', true);
    }

  }
});
