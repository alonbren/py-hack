import Ember from 'ember';

export default Ember.Component.extend({
  trigValid: 0,
  actions: {
    validateAll() {
      this.set('trigValid',! this.get('trigValid') );
    }
  }
});
