import Ember from 'ember';

const ErrorsTexts = {
    MINLENGTH: "The text should be at least %@ chars",
    MAXLENGTH: "The text should be max of %@ chars",
    REGEX: "The text is not valid",
    REG: "The text should not contain <, >, `,| , ~, ', \"",
    EMAIL: "The text is not valid Email address",
    REQUIRED: "Please fill the field",
    NUM: "Please insert numbers only",
    INVOICENUMBER: "The text is not valid invoice number",
    MINNUM: "Please insert number higher then %@",
    MAXNUM: "Please insert number lower then %@",
  },
  RegexList = {
    EMAIL: /^[a-zA-Z0-9.!#$%&’"*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/,
    NUM: /^\d+$/,
    REG: /^[^<>]*$/,
    INVOICENUMBER: /^[a-zA-Z0-9-_]+$/,
  };

export default Ember.Component.extend({
  elm: 'input',
  tagName: '',
  isValid: true,
  tempVal: null,
  invalidType: null,
  initialize: function(){
    this.setProperties({
      tempVal: this.value
    });
  }.on("init"),
  trigValid: Ember.observer('trigger-validate', function() {
    this.send('validate');
  }),
  type: 'text',
  area: false,
  placeholder: '',
  focusOutFn: () => {},
  changedFn: () => {},
  errorText: Ember.computed('isValid', 'invalidType', function() {
    if (!this.isValid ) {
      switch (this.invalidType) {
        case 'MINLENGTH':
          return Ember.String.loc(ErrorsTexts[this.invalidType], [this.minlength]);

        case 'MAXLENGTH':
          return Ember.String.loc(ErrorsTexts[this.invalidType], [this.maxlength]);

        case 'MINNUM':
          return Ember.String.loc(ErrorsTexts[this.invalidType], [this.min]);

        case 'MAXNUM':
          return Ember.String.loc(ErrorsTexts[this.invalidType], [this.max]);

        default:
          return ErrorsTexts[this.invalidType];
      }

    }
    return null;
  }),
  setInvalid: (type, sender) => {
    sender.set('isValid', false);
    sender.set('invalidType', type);
    sender.set('value', null);
    return;
  },
  actions: {
    valChanged() {
      if (!this.isValid) {
        this.send('validate');
      }
      this.changedFn();
    },
    validate() {
      const {setInvalid} = this;
      let _type = this.type.toLowerCase();

      if (! this.allowEmpty && ! this.tempVal) {
        return setInvalid('REQUIRED', this);
      }
      if (this.minlength) {
        if (this.minlength > this.tempVal.length) {
          return setInvalid('MINLENGTH', this);
        }
      }
      if (this.maxlength) {
        if (this.maxlength < this.tempVal.length) {
          return setInvalid('MAXLENGTH', this);
        }
      }
      switch (_type) {
        case 'email':
          if (!RegexList.EMAIL.test(this.tempVal)) {
            return setInvalid('EMAIL', this);
          }
          break;
        case 'number':
          if (!RegexList.NUM.test(this.tempVal)) {
            return setInvalid('NUM', this);

          }
          if (this.min) {
            if (this.min > this.tempVal) {
              return setInvalid('MINNUM', this);


            }
          }
          if (this.max) {
            if (this.max < this.tempVal) {
              return setInvalid('MAXNUM', this);


            }
          }
          break;
        case 'invoicenumber':
          if (!RegexList.INVOICENUMBER.test(this.value)) {
            this.set('isValid', false);
            this.set('invalidType', 'INVOICENUMBER');
            return;
          }
          break;
      }

      if (this.regex) {
        if (RegexList[this.regex.toUpperCase()]) {
          if (!RegexList[this.regex.toUpperCase()].test(this.tempVal) ) {
            return setInvalid(this.regex.toUpperCase(), this);

          }
        }
        else{
          let _reg = new RegExp(this.regex);
          if (!_reg.test(this.tempVal) ) {

            return setInvalid('REGEX', this);
          }

        }

      }


      this.set('isValid', true);
      this.set('value', this.tempVal);

      this.focusOutFn();
    }
  }
});
